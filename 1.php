<?php
/*第一种快速排序*/
function quick_sort(array $sort,$start,$end){
    $i;
    $j;
    $temp;
    if($start<$end){
        $i=$start;
        $j=$end+1;
        while(1){
            
            do{
                $i++;
            }while(!($sort[$start]<=$sort[$i] || $i==$end));
            
            
            do{
                $j--;
            }while(!($sort[$j]<=$sort[$start] || $j==$start));                                      
            
            
            if($i<$j){
                $temp=$sort[$i];
                $sort[$i]=$sort[$j];
                $sort[$j]=$temp;
            }else{
                break;
            }
               
        }
        $temp=$sort[$start];
        $sort[$start]=$sort[$j];
        $sort[$j]=$temp;
        quick_sort($sort,$start,$j-1);
        quick_sort($sort,$j+1,$end);

        
    }
    
   return $sort;
}


/*第二种快速排序*/
function quick_sort(array $array){
    $length=count($array);
    $left_array=array();
    $right_array=array();
    if($length<=1){
        return $array;
    }
    
    $key=$array[0];
    //注意这里的i一定要等于1，也就是从数组的第二个值开始比较，因为第一个值已经是被选定为对比值了
    for($i=1;$i<$length;$i++){
        if($array[$i]>$key){
            $right_array[]=$array[$i];
        }else{
            $left_array[]=$array[$i];
        }
    }
    
    $left_array=quick_sort($left_array);
    $right_array=quick_sort($right_array);
    return array_merge($left_array,array($key),$right_array);    
}
